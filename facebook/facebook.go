package facebook

import (
	fb "github.com/huandu/facebook"
)

func GetID(token string) string {
	res, err := fb.Get("/me", fb.Params{
		"access_token": token,
	})

	if err != nil {
		return ""
	}

	var id string
	res.DecodeField("id", &id)

	return id
}
