package users

import (
	log "github.com/Sirupsen/logrus"
	"github.com/gocql/gocql"
	"github.com/nu7hatch/gouuid"
)

const (
	HenHoKeyspace = "henho"
)

var RoomNames = [...]string{"Sữa", "Đường", "Gió", "Sông"}

type User struct {
	session *gocql.Session
}

type Room struct {
	ID   string `cql:"id"`
	Name string `cql:"name"`
}

func New(ip string) (*User, error) {
	cluster := gocql.NewCluster(ip)
	cluster.ProtoVersion = 4

	cluster.Keyspace = HenHoKeyspace
	cluster.Consistency = gocql.Quorum
	session, err := cluster.CreateSession()

	if err != nil {
		return nil, err
	}

	return &User{
		session,
	}, nil
}

// scanID scan for user id from facebook id
func (user *User) scanID(fbID string) (string, error) {
	id := ""

	if err := user.session.Query(`SELECT id FROM ids WHERE fb_id = ? LIMIT 1`,
		fbID).Consistency(gocql.One).Scan(&id); err != nil {
		return "", err
	}

	return id, nil
}

func (user *User) IDFromFBID(fbID string) string {
	// Scan for ID from Facebook ID
	id, err := user.scanID(fbID)
	if (err != nil) && (err.Error() != "not found") {
		return ""
	}

	if id != "" {
		return id
	}

	// Found no ID, create new ID for this Facebook ID
	if err := user.session.Query(`INSERT INTO ids (fb_id, id) VALUES (?, blobAsUuid(timeuuidAsBlob(now())))`,
		fbID).Exec(); err != nil {
		log.Error(err)
		return ""
	}

	// Scan again for newly created ID
	id, err = user.scanID(fbID)
	if err != nil {
		return ""
	}

	log.Infof("New user: %s", id)
	return id
}

// Call adds id into calling table
func (user *User) Call(id string) error {
	var sex string
	// Get user sex
	if err := user.session.Query(`SELECT sex FROM users WHERE id = ? LIMIT 1`,
		id).Consistency(gocql.One).Scan(&sex); err != nil {
		log.Error(err)
		return err
	}

	// Add user to calling table
	if err := user.session.Query(`INSERT INTO calling (id, sex) VALUES (?, ?)`,
		id, sex).Exec(); err != nil {
		log.Error(err)
		return err
	}

	return nil
}

// hang hangs up a user from a call
func (user *User) hang(id string) {
	if err := user.session.Query(`DELETE FROM calling WHERE id = ?`,
		id).Exec(); err != nil {
		log.Error(err)
	}
}

// Disconnect disconnect user from database
func (user *User) Disconnect(id string) {
	user.hang(id)

	pID := ""
	// Search for match
	if err := user.session.Query(`SELECT id2 FROM matches WHERE id1 = ? LIMIT 1`,
		id).Consistency(gocql.One).Scan(&pID); err != nil {
		log.Error(err)
	}
	if err := user.session.Query(`DELETE FROM matches WHERE id1 = ?`,
		id).Exec(); err != nil {
		log.Error(err)
	}

	if pID != "" {
		if err := user.session.Query(`DELETE FROM matches WHERE id1 = ?`,
			pID).Exec(); err != nil {
			log.Error(err)
		}
	}
}

// Match matches 2 users
func (user *User) Match(id1, id2 string) {
	user.session.Query(`INSERT INTO matches (id1, id2) VALUES (?, ?)`,
		id1, id2).Exec()
	user.session.Query(`INSERT INTO matches (id1, id2) VALUES (?, ?)`,
		id2, id1).Exec()
}

// MatchOf gets a match for a user
func (user *User) MatchOf(id string) string {
	pID := ""
	// Search for match
	if err := user.session.Query(`SELECT id2 FROM matches WHERE id1 = ? LIMIT 1`,
		id).Consistency(gocql.One).Scan(&pID); err != nil {
		log.Error(err)
	}

	// If there is no match yet, match this user with calling users
	if pID == "" {
		var sex string
		// Get current user sex
		if err := user.session.Query(`SELECT sex FROM users WHERE id = ? LIMIT 1`,
			id).Consistency(gocql.One).Scan(&sex); err != nil {
			log.Error(err)
			return ""
		}
		pSex := "m"
		if sex == "m" {
			pSex = "f"
		}

		// Search for opposite sex user from calling table
		if err := user.session.Query(`SELECT id FROM calling WHERE sex = ? LIMIT 1`,
			pSex).Consistency(gocql.One).Scan(&pID); err != nil {
			log.Error(err)
			return ""
		}
		user.hang(id)
		user.hang(pID)
		user.Match(id, pID)
	}
	log.Info(pID)

	return pID
}

func (user *User) addRoom(userID, roomID, roomName string) {
	rooms := []Room{Room{ID: roomID, Name: roomName}}
	if err := user.session.Query(`UPDATE users SET rooms = ? + rooms WHERE id = ?`,
		rooms, userID).Exec(); err != nil {
		log.Error(err)
	}
}

// getRoomNames gets all room names that user participates in
func (user *User) getRoomNames(userID string) (map[string]struct{}, error) {
	// Query all rooms that user participates in
	rooms := []Room{}
	if err := user.session.Query(`SELECT rooms FROM users WHERE id = ? LIMIT 1`,
		userID).Consistency(gocql.One).Scan(&rooms); err != nil {
		return nil, err
	}

	// Extract room names
	var roomNames map[string]struct{}
	for _, room := range rooms {
		roomNames[room.Name] = struct{}{}
	}

	return roomNames, nil
}

func (user *User) CreateRoom(userID1, userID2 string) string {
	// Generate room id
	roomUUID, err := uuid.NewV4()
	if err != nil {
		log.Error(err)
		return ""
	}
	roomID := roomUUID.String()

	// Get room names of both users
	roomNames, err := user.getRoomNames(userID1)
	if err != nil {
		return ""
	}
	roomNames1, err := user.getRoomNames(userID2)
	if err != nil {
		return ""
	}
	for roomName, _ := range roomNames1 { // Combine both set of room names into one
		roomNames[roomName] = struct{}{}
	}

	// Choose room names that not already existed in both users room names
	roomName := ""
	for _, name := range RoomNames {
		if _, ok := roomNames[name]; !ok {
			roomName = name
			break
		}
	}

	// Create new room
	if err := user.session.Query(`INSERT INTO rooms (id, name, user_ids) VALUES (?, ?, [?, ?])`,
		roomID, roomName, userID1, userID2).Exec(); err != nil {
		log.Error(err)
		return ""
	}

	user.addRoom(userID1, roomID, roomName)
	user.addRoom(userID2, roomID, roomName)

	return roomID
}
