package main

import (
	"github.com/seraphpl/henho/api"
	"github.com/seraphpl/henho/utils"
)

func main() {
	signingKey := utils.Getenv("HENHO_SIGNING_KEY", "TestKey")
	dbIp := utils.Getenv("HENHO_DB_IP", "192.168.99.100")
	port := utils.Getenv("HENHO_PORT", "3001")

	s := api.NewServer(signingKey, dbIp)
	s.Bind()
	s.Run(port)
}
