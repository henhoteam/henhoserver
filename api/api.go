// Package api is api server for henho
package api

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"

	fb "github.com/seraphpl/henho/facebook"
	"github.com/seraphpl/henho/middleware"
	"github.com/seraphpl/henho/users"
)

// APIServer is a struct for API server
type APIServer struct {
	*echo.Echo
	SigningKey string
	User       *users.User
}

// TokenResponse is a struct for responding token
type TokenResponse struct {
	Token string `json:"token"`
}

// StatusResponse is a struct for responding status
type StatusResponse struct {
	Status string `json:"status"`
}

// NewServer returns new API server
func NewServer(signingKey, dbIp string) *APIServer {
	user, err := users.New(dbIp)
	if err != nil {
		log.Fatal(err)
	}
	e := echo.New()
	e.Use(middleware.CORS())

	s := &APIServer{
		e,
		signingKey,
		user,
	}
	return s
}

// HandleLogin receives facebook token and returns JWT
func (s *APIServer) HandleLogin(c *echo.Context) error {
	fbToken := c.Form("fb_token")
	fbID := fb.GetID(fbToken)

	if fbID != "" {
		id := s.User.IDFromFBID(fbID)
		token := jwt.New(jwt.SigningMethodHS256)

		// Set a header and a claim
		token.Header["typ"] = "JWT"
		token.Claims["exp"] = time.Now().Add(time.Hour * 96).Unix()
		token.Claims["id"] = id

		// Generate encoded token
		t, _ := token.SignedString([]byte(s.SigningKey))
		return c.JSON(http.StatusOK, &TokenResponse{Token: t})
	} else {
		return c.JSON(http.StatusUnauthorized, &StatusResponse{Status: "unauthorized"})
	}
}

// HandleAPI handles API call for the server. Currently, just return "ok"
func (s *APIServer) HandleAPI(c *echo.Context) error {
	return c.JSON(http.StatusOK, &StatusResponse{Status: "ok"})
}

// HandleCall registers calling status
func (s *APIServer) HandleCall(c *echo.Context) error {
	claims, ok := c.Get("claims").(map[string]interface{})
	if !ok {
		return errors.New("Invalid claims")
	}

	id, ok := claims["id"].(string)
	if !ok {
		return errors.New("Invalid claims")
	}

	if err := s.User.Call(id); err != nil {
		return err
	}
	log.Infof("%s 's calling", id)

	return nil
}

// HandleLogin return match for an ID
func (s *APIServer) HandleMatch(c *echo.Context) error {
	return nil
}

// Bind routes path to API server handlers
func (s *APIServer) Bind() {
	// Restricted group
	r := s.Echo.Group("/api")
	r.Use(middleware.JWTAuth(s.SigningKey))
	r.Get("/call", s.HandleCall)

	s.Echo.Get("/login", s.HandleLogin)
	s.Echo.Get("/match", s.HandleMatch)
}

// Run runs API server
func (s *APIServer) Run(port string) {
	log.Printf("Listening on port %s", port)
	s.Echo.Run(fmt.Sprintf(":%s", port))
}
