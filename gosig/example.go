package main

import (
	"fmt"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"github.com/googollee/go-socket.io"

	"github.com/seraphpl/henho/users"
	"github.com/seraphpl/henho/utils"
)

type User struct {
	SocketID string
	ID       string
	LikedID  string // ID of partner that this user liked
}

type CustomServer struct {
	*socketio.Server
	Users      Users
	SigningKey string
	User       *users.User
}

type Users []*User

func (s *CustomServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	origin := r.Header.Get("Origin")
	w.Header().Set("Access-Control-Allow-Origin", origin)
	s.Server.ServeHTTP(w, r)
}

// LoginCallback handles login process
func (s *CustomServer) LoginCallback(so socketio.Socket) func(token string) {
	return func(token string) {
		// Validate token
		t, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
			// Always check the signing method
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			// Return the key for validation
			return []byte(s.SigningKey), nil
		})
		if err != nil || !t.Valid {
			log.Println("Log in failed.")
			so.Emit("login_error", err.Error())
			return
		}
		id, ok := t.Claims["id"].(string)
		if !ok {
			log.Println("Log in failed.")
			return
		}

		// If this socket is already connected, send a failed login message
		if s.Users.IndexBySocketID(so.Id()) != -1 {
			so.Emit("login_error", "You are already connected.")
			return
		}
		// If this id is already registered, send a faild login message
		if s.Users.IndexByID(id) != -1 {
			so.Emit("login_error", "This id already exists.")
			return
		}

		// Create a channel for this socket
		so.Join(so.Id())

		// Add this user
		s.Users = append(s.Users, &User{
			SocketID: so.Id(),
			ID:       id,
			LikedID:  "",
		})

		so.Emit("login_successful")
		log.Println(id + " logged in")
	}
}

// CallCallback handles call signal
func (s *CustomServer) CallCallback(so socketio.Socket) func() {
	return func() {
		userIndex := s.Users.IndexBySocketID(so.Id())
		if userIndex == -1 {
			return
		}
		user := s.Users[userIndex]

		log.Infof("%s is calling", user.ID)
		s.User.Call(user.ID)
	}
}

// partnerOf looks for partner of user that connected to server
func (s *CustomServer) partnerOf(user *User) *User {
	// Get ID of the match of current user
	pID := s.User.MatchOf(user.ID)
	if pID == "" {
		return nil
	}
	partnerIndex := s.Users.IndexByID(pID)
	if partnerIndex == -1 {
		return nil
	}
	partner := s.Users[partnerIndex]
	return partner
}

// SendMessageCallback handles sending message process
func (s *CustomServer) SendMessageCallback(so socketio.Socket) func(message interface{}) {
	return func(message interface{}) {
		userIndex := s.Users.IndexBySocketID(so.Id())
		if userIndex == -1 {
			return
		}
		currentUser := s.Users[userIndex]

		// Get ID of the match of current user
		pID := s.User.MatchOf(currentUser.ID)
		if pID == "" {
			return
		}
		contactIndex := s.Users.IndexByID(pID)
		if contactIndex == -1 {
			return
		}
		contact := s.Users[contactIndex]

		log.Println(message)
		so.BroadcastTo(contact.SocketID, "messageReceived", message)
	}
}

// DisconnectionCallback handles disconnection process
func (s *CustomServer) DisconnectionCallback(so socketio.Socket) func() {
	return func() {
		userIndex := s.Users.IndexBySocketID(so.Id())

		if userIndex != -1 {
			user := s.Users[userIndex]

			// Disconnect user in database
			s.User.Disconnect(user.ID)

			// Delete user
			s.Users[userIndex] = s.Users[len(s.Users)-1]
			s.Users[len(s.Users)-1] = nil
			s.Users = s.Users[:len(s.Users)-1]

			log.Println(user.ID + " disconnected")
		}
	}
}

// LikeCallback handles call signal
func (s *CustomServer) LikeCallback(so socketio.Socket) func() {
	return func() {
		userIndex := s.Users.IndexBySocketID(so.Id())
		if userIndex == -1 {
			return
		}
		user := s.Users[userIndex]

		// Get partner
		partner := s.partnerOf(user)
		if partner == nil {
			return
		}

		// partner not yet liked user
		if partner.LikedID != user.ID {
			user.LikedID = partner.ID
			return
		}

		// partner liked this user
		roomID := s.User.CreateRoom(user.ID, partner.ID)

		// Notice both users they are connected
		so.BroadcastTo(user.SocketID, "newRoom", roomID)
		so.BroadcastTo(partner.SocketID, "newRoom", roomID)
	}
}

func (s *CustomServer) Bind() {
	s.On("connection", func(so socketio.Socket) {
		log.Println("on connection")
		time.AfterFunc(1*time.Second, func() {
			userIndex := s.Users.IndexBySocketID(so.Id())
			if userIndex == -1 {
				so.Emit("disconnect")
			}
		})

		so.On("login", s.LoginCallback(so))
		so.On("call", s.CallCallback(so))
		so.On("like", s.LikeCallback(so))
		so.On("sendMessage", s.SendMessageCallback(so))
		so.On("disconnection", s.DisconnectionCallback(so))
	})
	s.On("error", func(so socketio.Socket, err error) {
		log.Println("error:", err)
	})
}

func (users Users) IndexBySocketID(socketID string) int {
	index := -1
	for i, user := range users {
		if user.SocketID == socketID {
			index = i
			break
		}
	}
	return index
}

func (users Users) IndexByID(id string) int {
	index := -1
	for i, user := range users {
		if user.ID == id {
			index = i
			break
		}
	}
	return index
}

func main() {
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	signingKey := utils.Getenv("HENHO_SIGNING_KEY", "TestKey")
	dbIp := utils.Getenv("HENHO_DB_IP", "192.168.99.100")
	user, err := users.New(dbIp)
	if err != nil {
		log.Fatal(err)
	}
	myServer := &CustomServer{
		server,
		Users{},
		signingKey,
		user,
	}
	myServer.Bind()

	http.Handle("/socket.io/", myServer)
	log.Println("Serving :3000...")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
